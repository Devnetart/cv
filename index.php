<?php
$i = 0; // Project counter for accordions

// PHP TREATMENT
$about = file_get_contents("./datas/about.json");
$languages = file_get_contents("./datas/languages.json");
$contact = file_get_contents("./datas/contact.json");
$skills = file_get_contents("./datas/skills.json");
$pro_exp = file_get_contents("./datas/pro-exp.json");
$school_exp = file_get_contents("./datas/school-exp.json");
$hobbies = file_get_contents("./datas/hobbies.json");

// Get datas in arrays
$json_about = json_decode($about, true);
$json_languages = json_decode($languages, true);
$json_contact = json_decode($contact, true);
$json_skills = json_decode($skills, true);
$json_pro = json_decode($pro_exp, true);
$json_school = json_decode($school_exp, true);
$json_hobbies = json_decode($hobbies, true);

// Splitter function to split numbers
function splitter($val)
{
  $str = (string) $val ;
  $splitted = explode(".",$str);
  $whole = (integer)$splitted[0] ;
  $num = (integer) $splitted[1];
  return array('whole' => $whole, 'num' => $num);
}

// Split phone number
$phonenumber = str_split($json_contact['tel'], 2);
if(array_key_exists(5, $phonenumber)){
    $phonenumber = 0 . str_split($phonenumber[1])[1] .".". $phonenumber[2] .".". $phonenumber[3] .".". $phonenumber[4] .".". $phonenumber[5];
}
else if(strlen(10)){
    $phonenumber =  $phonenumber[0] .".". $phonenumber[1] .".". $phonenumber[2] .".". $phonenumber[3] .".". $phonenumber[4];
}
?>
<!-- HTML STRUCTURE -->
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Curriculum Vitae de <?=$json_about['firstname']?> <?=strtoupper($json_about['lastname'])?></title>

    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="frameworks/bootstrap/dist/css/bootstrap.min.css">
    <!-- SELF STYLE -->
    <!-- <link rel="stylesheet" href="css/styles.css" media="screen"> -->
    <link rel="stylesheet" href="css/print.css"><!-- media="print" -->

</head>
<body>
    <header>
        <div class="media">
            <div class="text-center">
                <img class="d-flex align-self-start mr-3 roundedImg" src="img/photo 2-min.jpg" alt="Ma photo">
                <div class="col age">
                    <h2>25 Ans</h2>
                </div> 
            </div>

            <div class="media-body">
                <div class="media-head text-center mb-3">
                    <div id="primary-infos" class="col-11">
                        <h1 class="mb-0"><?=$json_about['firstname']?> <?=strtoupper($json_about['lastname'])?></h1>
                        <h2><?=$json_about['work-title']?></h2>
                    </div>
                </div>
                <div id="objectives">
                
                    <div id="fast-infos" class="text-center">
                        <div class="row icons">
                            <div class="col" 
                                data-toggle="popover" data-trigger="hover" title="<?=$json_about['graduated']?>"
                                data-content="<?=$json_about['graduated-text']?>">
                                <i class="fas fa-3x fa-graduation-cap"></i>
                                <h5>Diplômé</h5>
                            </div>

                            <div class="col">
                                <i class="material-icons md-48"
                                data-toggle="popover" data-trigger="hover" 
                                title="<?= $json_about['work-level']?>" data-content="<?=$json_about['work-text']?>">work</i>
                                <h5><?=$json_about['work-since']?> ans</h5>
                            </div>
                            
                            <div class="col">
                                <i class="material-icons md-48" 
                                data-toggle="popover" data-trigger="hover" title="<?=$json_about['mobility']?>"
                                data-content="<?=$json_about['mobility-text']?>">directions_car</i>
                                <h5>Véhiculé</h5>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <p class="mb-0">
                        D'un naturel curieux et audacieux, je prend plaisir à m'impliquer dans des projets IT et managériaux.
                        <br>Doté d'un esprit critique, je n'hésite pas à donner mon avis et à remettre en questions mes acquis et compétences.
                        <br>Minutieux et soucieux de qualité, je privilégie un code factorisé, optimsé & performant avec une vision à long termes, pour des solutions maintenables et évolutives.
                        
                    </p>
            </div>
        </div>
    </header>
    
    <div class="body d-flex flex-row">
        <section class="sidebar">
            <!-- CONTACTS -->
            <div id="contact" class="card">
                <div class="card-header text-center">
                    <h3>Contacts</h3>
                </div>
                <ul class="list-group" class="card-block">
                    <li class="list-group-item text-center" data-toggle="tooltip" data-placement="right"
                        title="<?=$json_contact['mail-desc']?>">
                        <i class="material-icons">email</i>
                        <a href="mailto:<?=$json_contact['mail']?>" class="card-link"><?=$json_contact['mail']?></a>
                    </li>    
                    <li class="list-group-item" data-toggle="tooltip" data-placement="right"
                        title="<?=$json_contact['tel-desc']?>">
                        <i class="material-icons">phone</i>
                        <a href="tel:<?=$json_contact['tel']?>" class="card-link"><?=$phonenumber?></a>
                    </li>
                    <li class="list-group-item" data-toggle="tooltip" data-placement="right"
                        title="<?=$json_contact['link-desc']?>">
                        <i class="fab fa-linkedin"></i>
                        <a href="<?=$json_contact['linkedin']?>" class="card-link">Devart</a>
                    </li>
                    <li class="list-group-item" data-toggle="tooltip" data-placement="right"
                        title="<?=$json_contact['adresse-desc']?>">
                        <i class="material-icons">home</i>
                        <a href="https://www.google.fr/maps/place/Paris/@48.858973,2.0675767,10z/data=!3m1!4b1!4m5!3m4!1s0x47e66e1f06e2b70f:0x40b82c3688c9460!8m2!3d48.856614!4d2.3522219" class="card-link"><?=$json_contact['adresse']?></a>
                    </li>
                </ul>
            </div>

            <!-- SKILLS -->
            <div id="skills" class="card">
                <div class="card-header text-center">
                    <h3>Compétences</h3>
                </div>
                <ul class="list-group" class="card-block">
                    <?php
                    foreach ($json_skills as $item) {
                    ?>
                    <li class="list-group-item">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: <?=$item['percent-level']?>%;" aria-valuenow="<?=$item['percent-level']?>" aria-valuemin="0" aria-valuemax="100"><?=$item['skill']?></div>
                        </div>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
                    
            <!-- LANGUAGES -->
            <div id="languages" class="card">
                <div class="card-header text-center">
                    <h3>Langues</h3>
                </div>
                <ul class="list-group">
                    <?php 
                    foreach ($json_languages as $item) {
                    ?>
                    <li class="list-group-item" 
                    data-toggle="popover" data-trigger="hover" title="<?= $item['textLevel']?>" 
                    data-content="<?= $item['description'] ?>">
                        <img class="flag" src="./img/flags/flag-<?=$item['flagcode']?>.png">
                        
                        <div class="language-level">
                            <?php
                            // Get level / 5
                            $level = $item['digitLevel'];
                            // Full stars = Unity
                            for ($i=0; $i < intval($level); $i++) { 
                            ?>
                                <i class="material-icons align-middle md-36">star</i>
                            <?php
                            }     
                            if(intval($level) < 5){
                                $splitted_level = 0;
                                $rest = (5 - intval($level));

                                // Half stars = a number after comma
                                try{
                                    $splitted_level = splitter($level);
                                    if($splitted_level['num'] != 0){
                                        $rest -= 1;
                                    ?>
                                        <i class="material-icons align-middle md-36">star_half</i>
                                    <?php
                                    }
                                }
                                catch (Exception $e){
                                    // Nothing to display
                                }
                                // Empty stars = calcul : (5 - Unity) ( - 1 if one half star )
                                for ($i=0; $i < $rest; $i++) { 
                                ?>
                                    <i class="material-icons align-middle md-36">star_border</i>
                                <?php
                                }
                            }
                            ?>
                        </div>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div id="hobbies" class="card">
                <?php
                foreach ($json_hobbies as $item) {
                ?>
                    <img data-toggle="popover" data-trigger="hover" title="<?= $item['hobbie']?>" 
                        data-content="<?= $item['description'] ?>"
                        src="<?=$item['img-path']?>">
                <?php
                }
                ?>
            </div>

        </section>
        
        <section class="content">
            <!-- PROFESSIONAL EXPERIENCES -->
            <div id="professionalExp" class="card  mb-3">
                <div class="card-header text-center">
                    <h4 class="mb-0">Expériences professionelles</h4>
                </div>
                <?php
                foreach ($json_pro as $item) {
                ?>
                <div class="card-body">
                    <h5 class="card-title"><?=$item['pro-title']?> - <?=$item['enterprise']?></h5>
                    <h6><?=$item['sub-title']?> - <?=$item['domain']?> <span class="badge badge-primary"><?=$item['duration']?></span></h6>
                    <p class="card-text"><?=$item['desc']?></p>

                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <?php
                        foreach ($item['projects'] as $project) {
                        // Increment i for each project
                            $i++;
                        ?>
                            <!-- One CARD by Project -->
                            <div class="card">
                                <div class="card-header" role="tab" id="heading<?=$i?>">
                                    <h5>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>" aria-expanded="true" aria-controls="collapse<?=$i?>">
                                            <?=$project['customer']?> <span class="badge badge-danger"><?=$project['duration']?></span>
                                        </a>
                                    </h5>
                                    <h6 class="mb-0"><?=$project['project']?></h6>
                                </div>
                                <div id="collapse<?=$i?>" class="collapse" role="tabpanel" aria-labelledby="heading<?=$i?>">
                                    <ul class="list-group list-group-flush">
                                    <?php
                                    foreach ($project['skills'] as $skill) {
                                    ?>
                                        <li class="list-group-item"><?=$skill?></li>
                                    <?php
                                    }
                                    ?>
                                    </ul>
                                </div>
                            </div>
                        <?php 
                        }
                        ?>
                    </div> <!-- End Accordion -->
                </div>
                <?php
                }
                ?>
            </div> <!-- END EXPERIENCES CARD -->

            <div id="schollExp" class="card">
                <div class="card-header text-center">
                    <h4 class="mb-0">Formation / Scolarité</h4>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?=$json_school['title']?> - <?=$json_school['school']?></h5>
                    <h6><?=$json_school['certified']?> <span class="badge badge-primary"><?=$json_school['duration']?></span></h6>
                    <p class="card-text"><?=$json_school['desc']?></p>
                </div>
            </div>            
        </section>
    </div> <!-- END BODY DIV -->
    
    <footer>
        <!-- LOAD JS SCRIPTS -->
        <script src="frameworks/jquery/jquery-2.1.3.min.js"></script>
        <!-- <script src="frameworks/jquery/jquery-3.3.1.min.js"></script> -->
        <script src="frameworks/popper.js/popper.min.js"></script>
        <script src="frameworks/tether.js/tether.min.js"></script>
        <script src="frameworks/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FONT AWESOME CDN -->
        <!-- <script defer src="frameworks/fontawesome/all.js"></script> -->
        <!-- Utils script
                - POPOVERS
                - TOOLTIPS
        -->
        <script src="js/utils.js"></script>

    </footer>
</body>
</html>

