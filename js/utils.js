/**
 * This script requires Jquery & Bootstrap
 */

// DISPLAY POPOVER
$(function () {
    $('[data-toggle="popover"]').popover()
})

// DISPLAY TOOLTIP
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})